from django.db import models

# Create your models here.

class Employee(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=10)
    address = models.CharField(max_length=100)
    phone = models.CharField(max_length=12)

    def __str__(self):
        return self.name
