from django.http import JsonResponse
from django.shortcuts import render

from rest_framework.response import Response
from rest_framework.decorators import api_view

from .models import Student

from .serializers import StudentSerializer

# Create your views here.

@api_view(['GET'])
def index(request):
    students = Student.objects.all()
    serializer_students = StudentSerializer(students, many=True)
    return Response(serializer_students.data)

@api_view(['GET'])
def StudentView(request, pk):
    student = Student.objects.get(id=pk)
    serializer_student = StudentSerializer(student, many=False)
    return Response(serializer_student.data)

@api_view(['POST'])
def AddStudent(request):
    serializer_data = StudentSerializer(data=request.data)
    if serializer_data.is_valid():
        serializer_data.save()
    
    return Response(serializer_data.data)

@api_view(['POST'])
def UpdateView(request, pk):
    student = Student.objects.get(id=pk)
    serializer = StudentSerializer(instance=student, data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['DELETE'])
def DeleteView(request, pk):
    student = Student.objects.get(id=pk)
    student.delete()

    students = Student.objects.all()
    serial_students = StudentSerializer(students, many=True)
    return Response(serial_students.data)
