from django.urls import path
from .import views

urlpatterns = [
    path('', views.index, name='index'),
    path('view-student/<str:pk>/', views.StudentView, name="student_view"),
    path('add-student/', views.AddStudent, name="add_student"),
    path('update-student/<str:pk>/', views.UpdateView, name="update_student"),
    path('delete-student/<str:pk>/', views.DeleteView, name="delete_student"),


]