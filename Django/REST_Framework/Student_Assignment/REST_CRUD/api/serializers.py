# from pyexpat import model
from rest_framework import serializers

from .models import Student, StudentGrade


class StudentGradeSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentGrade
        fields = ['grade', 'status']

class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ['student_grade', 'name', 'address', 'phone', 'father_name']
        depth = 1

        ## While adding new data we should remove depth = 1