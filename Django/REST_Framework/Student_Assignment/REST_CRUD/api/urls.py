from django.urls import path
from .import views

urlpatterns = [
    path('', views.StudentListView.as_view(), name='student-list'),
    path('create/', views.StudentCreateView.as_view(), name='student-create'),
    path('retrive/<str:pk>', views.StudentRetriveView.as_view(), name='student-retrive'),
    path('update/<str:pk>', views.StudentUpdateView.as_view(), name='student-update'),
    path('delete/<str:pk>', views.StudentDeleteView.as_view(), name='student-delete'),


]