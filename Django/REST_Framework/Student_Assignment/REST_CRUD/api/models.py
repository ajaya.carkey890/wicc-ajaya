from django.db import models

# Create your models here.

class StudentGrade(models.Model):
    grade = models.CharField(max_length=10)
    status = models.CharField(max_length=10)

    def __str__(self):
        return self.status


class Student(models.Model):
    student_grade = models.ForeignKey(StudentGrade, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    phone = models.CharField(max_length=12)
    father_name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

        
