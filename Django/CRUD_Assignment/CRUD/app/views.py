from django.shortcuts import get_object_or_404, redirect, render
from django.http import HttpResponse
from .forms import EmployeeForm

from .models import Employee

from django.contrib import messages

# Create your views here.

def employee_list(request):
    employee = Employee.objects.all()

    context = {
        'employee': employee
    }

    return render(request, 'employee_list.html', context)

def create_employee(request):

    form = EmployeeForm()

    if request.method == 'POST':
        form = EmployeeForm(request.POST)
        if form.is_valid():
            form.save()
            print("hello")
            #print(form.cleaned_data.get('employee_name'))
            username = form.cleaned_data.get('employee_name')
            messages.success(request, f'successfully created {username}')
            return redirect('employee_list')

    context = {
        'form': form,
    }
    return render(request, 'create_employee.html', context)


def edit_employee(request, pk):
    employee = Employee.objects.get(id=pk)
    form = EmployeeForm(instance=employee)

    if request.method == 'POST':
        form = EmployeeForm(request.POST, instance=employee)
        if form.is_valid():
            form.save()
            messages.success(request, f'successfully Updated User')
            return redirect('employee_list')
    context = {
        'employee': employee,
        'form': form
    }

    return render(request, 'edit_employee.html', context)

def employee_details(request, pk):
    #employee = get_object_or_404(Employee, id=pk)
    employee = Employee.objects.get(id=pk)

    context = {
        'employee': employee
    }
    return render(request, 'employee_details.html', context)
    

def delete_employee(request, pk):
    employee = Employee.objects.get(id=pk)

    if request.method == 'POST':
        employee.delete()
        return redirect('employee_list')

    context = {
        'employee': employee
    }
    return render(request, 'delete_employee.html', context)




