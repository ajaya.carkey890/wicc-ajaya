from django.db import models

# Create your models here.

class Employee(models.Model):
    employee_name = models.CharField(max_length=100)
    employee_address = models.CharField(max_length=100)
    employee_email = models.EmailField()
    employee_salary = models.IntegerField()


    def __str__(self):
        return self.employee_name
