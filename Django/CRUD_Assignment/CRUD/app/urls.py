from django.urls import path
from .import views


urlpatterns = [
    path('', views.employee_list, name='employee_list'),
    path('create/', views.create_employee, name='create_employee'),
    path('details<pk>/', views.employee_details, name='employee_details'),
    path('edit<pk>/', views.edit_employee, name='edit_employee'),
    path('delete<pk>/', views.delete_employee, name='delete_employee'),
    

]