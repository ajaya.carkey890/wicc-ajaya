from django.urls import path
from .import views

urlpatterns = [
    # path('', views.index, name='index')
    path('', views.IndexClass.as_view(), name='index'),
    path('list/', views.EmployeeListView.as_view(), name='employee_list'),
    path('create/', views.EmployeeCreateView.as_view(), name='create_employee'),
    path('details<pk>/', views.EmployeeDetailView.as_view(), name='employee_detail'),
    path('update<pk>/', views.EmployeeUpdateView.as_view(), name='employee_update'),
    path('delete<pk>/', views.EmployeeDeleteView.as_view(), name='employee_delete'),


]

