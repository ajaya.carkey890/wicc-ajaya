from audioop import reverse
from django import http
from django.shortcuts import render
from django.urls import reverse_lazy

from django.views.generic import TemplateView, ListView, CreateView, DetailView, UpdateView, DeleteView

from django.core.exceptions import PermissionDenied
from .models import Employee
from .forms import EmployeeForm
# Create your views here.

# def index(request):
#     return render(request, 'index.html')

class IndexClass(TemplateView):
    template_name = 'index.html'


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        #context['employee'] = Employee.objects.get(id=1)  ## calling from the database
        context['extra_info'] = "This is the Index page" ## adding extra contexts
        return context


class EmployeeListView(ListView):
    model = Employee
    template_name = 'employee_list.html'   ### In templates we have to loop in 'objects_list'>> default

    context_object_name = "employees" ## defining context object name which can be access in template
    paginate_by = 1
    #queryset = Employee.objects.all()[:2]  ## Returns only 2 objects
    #ordering = '-id'
    allow_empty = True
    http_method_names = ['get'] ## If we provide post method error will come as by default get is called



    def get_queryset(self):
        return Employee.objects.all()


    ## If user is not login in django admin then it will throw error 403 Forbidden
    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return super().dispatch(request, *args, **kwargs)
        raise PermissionDenied

    ## adding extra contents
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['extra_info'] = "Added employee will be in the last page" ## adding extra contexts
        return context


class EmployeeCreateView(CreateView):
    model = Employee
    template_name = 'create_employee.html'
    form_class = EmployeeForm ## if we donot specify this django throws error 
    #fields = '__all__'  ## if we donot specify this django throws error
    #http_method_names = ['get', 'post']
    success_url = '/list/'

    ## the value will be placed initially in the form fields

        # initial = {
    #     'name': 'Asbin',
    #     'age': 25,
    #     'address': 'Bhojpur',
    # }

    ## this method will call first from the url if this is consider we donot have to use above two
    # form_class & fields
    # def get_form_class(self):
    #     return EmployeeForm

    ## Same as above initial
    def get_initial(self):
        initial = super().get_initial()
        initial['name'] = 'Asbin'
        initial['age'] = 25
        initial['address'] = 'Bhojpur'
        return initial

    # def get_form_kwargs(self):
    #     kwargs = super().get_form_kwargs()
    #     kwargs['users'] = self.request.user 
    #     return kwargs


class EmployeeDetailView(DetailView):
    template_name = 'employee_details.html'
    model = Employee
    #pk_url_kwarg = 'pk'
    context_object_name = 'employee'


class EmployeeUpdateView(UpdateView):
    template_name = 'employee_update.html'
    model = Employee
    form_class = EmployeeForm
    pk_url_kwagrs = 'pk'
    success_url = reverse_lazy('employee_list')


class EmployeeDeleteView(DeleteView):
    template_name = 'employee_delete.html'
    model = Employee
    pk_url_kwargs = 'pk'
    context_object_name = 'employee'
    success_url = reverse_lazy('employee_list')




