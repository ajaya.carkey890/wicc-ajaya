from email.policy import default
from django.db import models


#Create your models here.

rating_fields = (
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5')

)


class Students(models.Model):
    student_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    password = models.CharField(max_length=100)
    phn_number = models.IntegerField()
    address = models.CharField(max_length=100)
    faculty = models.CharField(max_length=100)
    semester = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Books(models.Model):
    book_code = models.AutoField(primary_key=True)
    genre = models.CharField(max_length=100)
    quantity = models.IntegerField()
    book_name = models.CharField(max_length=100)
    authors = models.CharField(max_length=100)
    ratings = models.CharField(max_length=100, choices= rating_fields, default=1)
    students = models.ManyToManyField(Students, through='StudentsLog')


    def __str__(self):
        return self.book_name

class StudentsLog(models.Model):
    student = models.ForeignKey(Students, on_delete=models.CASCADE)
    book = models.ForeignKey(Books, on_delete=models.CASCADE)
    issue_date = models.DateField()
    expiry_date = models.DateField()
    no_of_days = models.CharField(max_length=100)
    fine = models.CharField(max_length=100, default='0')

    class Meta:
        unique_together = [['student', 'book']]


