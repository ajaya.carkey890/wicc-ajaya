from django.shortcuts import render
#from django.http import HttpResponse
# Create your views here.


def index(request):
    return render(request, 'index.html')



student_details = [
    {
        'id': '101',
        'name': 'Ajaya Karki',
        'roll_no': '1',
        'address': "Koteswor",
        'mobile_number': "9862192243",
        'email': 'ajaya.carkey890@gmail.com',
    },
    {
        'id': '102',
        'name': 'Lakpa Sherpa',
        'roll_no': '5',
        'address': 'Jorpati',
        'mobile_number': '9865478892',
        'email': 'lakpa.sherpa12@gmail.com',
    },
    {
        'id': '103',
        'name': 'Babin Tamang',
        'roll_no': '8',
        'address': 'Kapan',
        'mobile_number': '9865952210',
        'email': 'babin.tamang12@gmail.com',
    },
    {
        'id': '104',
        'name': 'Supriya Shakya',
        'roll_no': '10',
        'address': 'Samakoshi',
        'mobile_number': '9865489951',
        'email': 'supriya.shakya22@gmail.com',
    },
    {
        'id': '105',
        'name': 'Sagun joshi',
        'roll_no': '6',
        'address': 'Maharajgunj',
        'mobile_number': '9865484123',
        'email': 'sagun.shakya76@gmail.com',
    },
    {
        'id': '106',
        'name': 'Saroj Neupane',
        'roll_no': '2',
        'address': 'Kapan',
        'mobile_number': '9862142260',
        'email': 'saroj.neupane33@gmail.com',
    },
    {
        'id': '107',
        'name': 'Zidhan Pradhan',
        'roll_no': '9',
        'address': 'Pepsicola',
        'mobile_number': '9858741120',
        'email': 'zidhan.pradhan30@gmail.com',
    },
    {
        'id': '108',
        'name': 'Swastika Shrestha',
        'roll_no': '7',
        'address': 'Bhojpur',
        'mobile_number': '9852147890',
        'email': 'swastika121@gmail.com',
    },
    {
        'id': '109',
        'name': 'Nirvic Basnet',
        'roll_no': '4',
        'address': 'Koteswor',
        'mobile_number': '98654400158',
        'email': 'nirvic.basnet22@gmail.com',
    },
    {
        'id': '110',
        'name': 'Bibash Rai',
        'roll_no': '3',
        'address': 'Swayambhu',
        'mobile_number': '9865400214',
        'email': 'bibash.rai87@gmail.com',
    },
   
]


def details(request):

    context = {
        'student_details': student_details,
    }

    return render(request, 'details.html', context)