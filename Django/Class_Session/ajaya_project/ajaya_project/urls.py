"""ajaya_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from unicodedata import name
from django import views
from django.contrib import admin
from django.urls import path
from .views import AboutUsView, home_view, about_view, contact_view, article_view, ExpenseListView,email_sent

urlpatterns = [
    path('admin/', admin.site.urls),

    path('',home_view),
    path('about/', AboutUsView.as_view()),

    #path('about/', about_view),


    path('contact/', contact_view),

    path('article/', article_view),

    path('expense-list/', ExpenseListView.as_view()),

    ## For Email
    path('email/', email_sent, name='email_sent'),
]
