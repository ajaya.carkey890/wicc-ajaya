import email
from pickle import GET
from tkinter.tix import Form
from django.http import HttpResponse
from django . shortcuts import render, redirect
from django.views import View

from .forms import AboutUsForm, ArticleForm

from django.views.generic import FormView

from django.core.mail import send_mail

from django.contrib import messages



article_list = [
    {
        "title": "First article",
        "content": "This is the first article",
        "date": "2022-1-2",
    },
    {
        "title": "Second article",
        "content": "This is the Second article",
        "date": "2022-1-3",
    },
    {
        "title": "Third article",
        "content": "This is the Third article",
        "date": "2022-1-4",
    },
    {
        "title": "Fourth article",
        "content": "This is the Fourth article",
        "date": "2022-1-5",
    },
    ]


def home_view(request):

    context = {
        'name': "Ajaya",
        'address': "Koteswor",
        'age': 21,
        "about_job": {
            "company": "InfoDevelopers",
            "Location": "Ktm",
        },

        'interests': ['Football', 'Cricket']
    }

    return render(request, 'home.html', context) 


class AboutUsView(FormView):
    form_class = AboutUsForm
    template_name = 'about.html'

    def form_invalid(self, form):
        print("This form is invalid")
        return super().form_invalid(form)

    def form_valid(self, form):
        print("Post Request", self.request.POST)
        form = AboutUsForm(self.request.POST)
        name = self.request.POST.get('title')
        desc = self.request.POST.get('description')
        print(name, desc, "OKKKKKKKKKK")
        print("This is POST request")
        return render(self.request, 'about.html', {'form': form})







def about_view(request):

    form = AboutUsForm()

    if request.method == "GET":
        name = request.GET.get('title')
        desc = request.GET.get('description')
        print("This is Get request")
        print(name, desc, "GET Doneeeeee")
        context = {
            'form': form
        }
        return render(request, 'about.html', context)
    else:
        print("Post Request")
        form = AboutUsForm(request.POST)

        if form.is_valid():
            name = request.POST.get('title')  ## title because post request is form and it's name is 'title'
            desc = request.POST.get('description')  ## same description
            print("This is POST request")
            print(name, desc, "POST Doneeeeee")
        return render(request, 'about.html', {'form': form})
        #return redirect("/")

    print(request.method , "OKOKOKOKOKOK")

    return render(request, 'about.html')

article = []
def article_view(request):

    form = ArticleForm()

    if request.method == "GET":
        #article_name = request.GET.get('article_name')
        #article_description = request.GET.get('article_description')
        print("This is Get request")
        #print(article_name, article_description, "GET Doneeeeee")
        context = {
            'form': form,
            'article_list': article_list,
            'article': article
        }
        return render(request, 'article_list.html', context)
    
    else:
        print("Hello Hello Not valid")
    
        form = ArticleForm(request.POST)

        if form.is_valid():
            article_name = request.POST.get('article_name')
            article_description = request.POST.get('article_description')
            #form.save()
            print("Yeah, This is Post request")
            print(article_name, article_description, "Ow Yeahhhh")
        #return render(request, 'about.html', {'form': form})

            articles = {
            'article_name': article_name,
            'article_description' : article_description 
    }

            article.append(articles)
    

    context = {
        'article_list': article_list,
        'form': form,
        'article': article
        
    }

    return render(request, 'article_list.html', context)


def contact_view(request):

    if request.method == "POST":
        name1 = request.POST.get('name')
        email1 = request.POST.get('email')

        context = {
            'name1': name1,
            'email1': email1
        }
        return render(request, 'contact.html', context)
    
    else:
        if request.method == 'GET':
            context = {
                'name': "Ajaya",
                'address': "Koteswor",
                'age': 21,
                "about_job": {
                    "company": "InfoDevelopers",
                    "Location": "Ktm",
        },

        'interests': ['Football', 'Cricket']
    }

    return render(request, 'contact.html', context)


expenses = []
class ExpenseListView(View):
    def get(self,request):
        context = {
            "expenses": expenses
        }

        return render(request, 'expense.html', context)

    def post(self, request):
        title = request.POST.get("name")
        amount = request.POST.get("amount")

        print("title", title)
        print("amount", amount)
        new_expense = {
            "title": title,
            "amount": amount
        }

        expenses.append(new_expense)

        return redirect('/expense-list/')



### Sending Email

def email_sent(request):

    if request.method == 'POST':
        name = request.POST.get('name')
        usr_email = request.POST.get('email')
        subject = request.POST.get('subject')
        message = request.POST.get('message')

        print(name, usr_email, subject, message)
        usr_email = [usr_email]

        send_mail(subject,message,'ajcarkey1@gmail.com',usr_email)
        messages.success(request, f'The email has been sent to {usr_email} check you mail')
        return render(request, 'email.html')

    return render(request, 'email.html')


