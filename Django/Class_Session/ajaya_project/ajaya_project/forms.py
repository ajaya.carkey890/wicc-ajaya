from pyexpat import model
#from attr import fields
from django import forms
from django.core.exceptions import ValidationError


from .import views

from WICC.models import ArticleModel, AboutUsModel



TITLES = ['WICC', 'Python', 'Django']


class AboutUsForm(forms.Form):
    title = forms.CharField(max_length=500, required=True, label="name")
    description = forms.CharField(widget=forms.Textarea)

    def __init__(self, *args, **kwargs):
        super(AboutUsForm, self).__init__(*args, **kwargs)

        # fields = ['title', ''description]
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'}) ## updates all that falls under class 'form-control'

        self.fields['title'].widget.attrs.update({'placeholder': 'Enter a title'})
        self.fields['description'].widget.attrs.update({'placeholder':'Enter a discription'})

    def clean(self):
        cleaned_data = super().clean()
        title = self.cleaned_data.get('title')
        description = self.cleaned_data.get('description')
        if title in TITLES:
            raise ValidationError("Its already exist")
        if '@' in description:
            raise ValidationError("@ is not acceptable")

        return cleaned_data



########################################### ModelForm Concept ##########################################################

# class ArticleForm(forms.ModelForm):

#     article_description = forms.CharField(widget=forms.Textarea)
#     class Meta:
#         model = ArticleModel
#         fields = '__all__'

#     def __init__(self, *args, **kwargs):
#         super(ArticleForm, self).__init__(*args, **kwargs)

#         self.fields['article_name'].widget.attrs.update({'placeholder': 'Article Title', 'class': 'form-control'})
#         self.fields['article_description'].widget.attrs.update({'class': 'form-control', 'rows': 10,'placeholder': 'Article Description',})

#     def clean(self):
#         cleaned_data = super().clean()
#         article_name = self.cleaned_data.get('article_name')
#         article_description = self.cleaned_data.get('article_description')

#         article_lower = article_name.lower()


#         for attr in views.article_list:
#             if article_lower == attr['title'].lower():
#                 raise ValidationError("Sorry title already exist")

#         return cleaned_data



########################################## Form Concept ###############################################################

class ArticleForm(forms.Form):
    article_name = forms.CharField(max_length=500, required=True, label="name")
    article_description = forms.CharField(widget=forms.Textarea)


    def __init__(self, *args, **kwargs):
        super(ArticleForm, self).__init__(*args, **kwargs)

    ## fields = ['article_name, 'article_description]
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})

        self.fields['article_name'].widget.attrs.update({'placeholder': 'Article Title'})
        self.fields['article_description'].widget.attrs.update({'placeholder': 'Article Description'})


    def clean(self):
        cleaned_data = super().clean()
        article_name = self.cleaned_data.get('article_name')
        article_description = self.cleaned_data.get('article_description')

        article_lower = article_name.lower()


        for attr in views.article_list:
            if article_lower == attr['title'].lower():
                raise ValidationError("Sorry title already exist")
            
            
        
        # if article_name in TITLES:
        #     raise ValidationError("Its already exist")
        
        

        return cleaned_data






