from django.db import models

# Create your models here.

class AboutUsModel(models.Model):
    title = models.CharField(max_length=100, default='', null=False, blank=False)
    description = models.CharField(max_length=1000, null=False, blank=False)

    def __str__(self):
        return self.title

class ArticleModel(models.Model):
    article_name = models.CharField(max_length=100, default='', null=False, blank=False)
    article_description = models.CharField(max_length=1000, null=False, blank=False)

    def __str__(self):
        return self.article_name
