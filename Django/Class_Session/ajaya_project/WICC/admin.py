from django.contrib import admin

from .models import *

# Register your models here.

admin.site.register(AboutUsModel)
admin.site.register(ArticleModel)

