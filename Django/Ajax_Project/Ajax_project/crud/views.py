from django.http import JsonResponse
from django.shortcuts import render
from .forms import EmployeeForm
from .models import Employee

# Create your views here.

def index(request):
    form = EmployeeForm()
    employee = Employee.objects.all()
    context = {
        'form': form,
        'employee': employee
    }
    return render(request, 'index.html', context)

def save_data(request):
    if request.method == 'POST':
        form = EmployeeForm(request.POST)
        if form.is_valid():
            form.save()
            employee_ = Employee.objects.values()
            #print(employee_)
            employee_data = list(employee_)
            return JsonResponse({'status': 'Save', 'employee_data':employee_data })
    else:
        return JsonResponse({'status': 0})


def edit_data(request):
    if request.method == 'POST':
        id = request.POST.get('sid')
        #print(id)
        employee = Employee.objects.get(pk=id)
        employee.save()
        employee_ = {"id": employee.id, "name": employee.name, "age": employee.age, "address": employee.address, "salary": employee.salary}
        #print(employee_)
        #employee_data = list(employee_)
        return JsonResponse(employee_)


def delete_data(request):
    if request.method == 'POST':
        id = request.POST.get('sid')
        employee = Employee.objects.get(pk=id)
        employee.delete()
        return JsonResponse({'status':1})
    else:
        return JsonResponse({'status':0})